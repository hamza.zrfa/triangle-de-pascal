#include <stdio.h>
#include <gmp.h>

void print_pascal_triangle(int n) {
    mpz_t c[n][n];
    int i, j;

    // Initialize GMP integers
    for (i = 0; i < n; i++) {
        for (j = 0; j <= i; j++) {
            mpz_init(c[i][j]);
        }
    }

    // Compute Pascal's triangle
    for (i = 0; i < n; i++) {
        for (j = 0; j <= i; j++) {
            if (j == 0 || j == i) {
                mpz_set_ui(c[i][j], 1);
            } else {
                mpz_add(c[i][j], c[i-1][j-1], c[i-1][j]);
            }
        }
    }

    // Print Pascal's triangle
    for (i = 0; i < n; i++) {
        for (j = 0; j <= i; j++) {
            mpz_out_str(stdout, 10, c[i][j]);
            printf(" ");
        }
        printf("\n");
    }
}

int main() {
    print_pascal_triangle(500);
    
    return 0;
}
